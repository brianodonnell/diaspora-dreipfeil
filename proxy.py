#!/usr/bin/python3
"""
A simple proxy to block incoming posts and comments from blacklisted
Diaspora* users.

author: Brian Ó Donnell <brian@pancrypticon.net>
license: MIT
"""

import argparse, base64, logging, re, socket, sys, threading

class FilteringProxy(object):
    """
    A proxy server that intercepts POST requests from other pods and silently
    drops content from users on a local blacklist
    """

    def __init__(self, args):
        self.args = args
        self.blacklist = self.get_blacklist(self.args.blacklist)

    def start(self):
        logging.info(
            "### Starting dreipfeil on %s:%s. Proxying to Diaspora* on %s:%s" %
            (
                self.args.bind_address,
                self.args.bind_port,
                self.args.diaspora_host,
                self.args.diaspora_port,
            )
        )
        logging.info("Blacklisted users (%s):" % len(self.blacklist))
        for user in sorted(self.blacklist):
            logging.info("  - %s" % user)
        self.listen()

    def get_blacklist(self, filename):
        """
        Extracts a list of Diaspora* user names from the file
        """
        blacklist = []
        with open(filename, 'rb') as file:
            for line in file.readlines():
                blacklist.append(line.decode().rstrip())
        return blacklist

    def listen(self):
        """
        Listens for incoming connections and filters them based on content
        """
        try:
            # sets up the listener
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind((self.args.bind_address, self.args.bind_port))
            s.listen(50)
        except Exception as e:
            print(e)
            sys.exit(2)

        while True:
            for th in threading.enumerate():
                if not th.is_alive():
                    th.join()
            logging.debug("Threads: %s" % threading.active_count())
            try:
                # gets data from the request
                connection, addr = s.accept()
                data = connection.recv(4096)
                if len(data) > 0:
                    # checks the request content, returns True if the content
                    # should be blocked
                    if self.filter(data, blacklist=self.blacklist):
                        # FAKE NEWS!!
                        connection.send(self.fake_response())
                    else:
                        # spin off a new thread to proxy the request to D*
                        t = threading.Thread(
                            target=self.proxy_connection,
                            args=(connection, data)
                        )
                        t.start()
            except KeyboardInterrupt:
                s.close()
                sys.exit(2)
        s.close()

    def fake_response(self):
        """
        Fakes a 200 response so the sender does not retry
        """
        return "HTTP/1.1 200 OK\n\nFAKE NEWS!!!\n".encode()

    def filter(self, data, blacklist=[]):
        """
        Extracts the author from the encoded data and returns True if it matches
        a blacklist entry
        """
        str_data = data.decode()
        first_line = str_data.split("\n")[0]
        # only inspect POST requests to /receive/public
        m1 = re.match('POST \/receive\/public', first_line)
        if m1:
            # decode the base64-encoded data
            m2 = re.search(
                '\<me:data type="application\/xml"\>(.*)\<\/me:data\>',
                str_data
            )
            if m2:
                decoded = str(base64.urlsafe_b64decode(m2.group(1)))
                m3 = re.search(
                    '\<(status_message|comment|like|reshare)\>',
                    decoded
                )
                if m3:
                    post_type = m3.group(1)
                    # ignore content that's not a post or a comment
                    m4 = re.search(r'\<author\>([A-Za-z0-9+-_\.@]+)\<\/author\>', decoded)
                    if m4:
                        # check the author against the blacklist
                        author = m4.group(1)
                        if author in blacklist:
                            logging.info(
                                "BLOCKED: %s [%s]" % (author, post_type)
                            )
                            return True
                        else:
                            logging.debug(
                                "ALLOWED: %s [%s]" % (author, post_type)
                            )
        return False

    def proxy_connection(self, connection, data):
        """
        Sends the request on to the Diaspora* server and returns its response
        """
        try:
            # opens a connection to the Diaspora* server
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.args.diaspora_host, self.args.diaspora_port))
            s.settimeout(30)
            # sends along the request
            s.send(data)
            # receives the response
            while True:
                reply = s.recv(4096)
                if len(reply) > 0:
                    connection.send(reply)
                else:
                    # s.close()
                    # connection.close()
                    # break
                    sys.exit(0)
        except KeyboardInterrupt:
            s.close()
            sys.exit(2)
        except Exception as e:
            print(e)
            s.close()
            connection.close()
            sys.exit(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--bind-address',
        help="IP to bind to (default=127.0.0.1)", default='127.0.0.1')
    parser.add_argument('--bind-port',
        help='TCP port to listen on (default=3001)', default=3001, type=int)
    parser.add_argument('--diaspora-host',
        help='Address of the D* server (default=127.0.0.1)',
        default='127.0.0.1')
    parser.add_argument('--diaspora-port',
        help='TCP port of the D* server (default=3000)', default=3000, type=int)
    parser.add_argument('--blacklist',
        help='Filename of the blacklist (default=blacklist)',
        default='blacklist')
    parser.add_argument('--log',
        help="Log file (default=dreipfeil.log)", default='dreipfeil.log')
    parser.add_argument('--loglevel',
        help="DEBUG, INFO, WARNING, ERROR, CRITICAL (default=INFO)",
        default='INFO')
    args = parser.parse_args()

    # set logging options
    log_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(log_level, int):
        raise ValueError('Invalid log level: %s' % args.loglevel)
    logging.basicConfig(
        filename=args.log,
        level=log_level,
        format='%(asctime)s %(message)s',
    )

    proxy = FilteringProxy(args)
    proxy.start()
