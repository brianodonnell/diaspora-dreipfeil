#!/bin/bash

/usr/sbin/useradd --home-dir /opt/dreipfeil --create-home --system dreipfeil
/usr/bin/apt-get install -y python3
/bin/cp proxy.py /opt/dreipfeil/
/usr/bin/touch /opt/dreipfeil/blacklist
/bin/mkdir -m 755 -p /var/log/dreipfeil
/bin/chown -R dreipfeil.dreipfeil /opt/dreipfeil /var/log/dreipfeil
/bin/cp systemd/dreipfeil.service /etc/systemd/system/
/bin/systemctl daemon-reload
/bin/systemctl enable dreipfeil
/bin/systemctl start dreipfeil
