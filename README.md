# Dreipfeil README
The Federation is an awesome, free, and open network, but it has its share of
trolls, fascists, and fascist trolls. Podmins can ban users on their own pods,
but there's no way to ban content from a user on another pod. That's where this
comes in to play. Dreipfeil runs as a thin proxy layer between your front-end
web server and your Diaspora* server. It blocks federated traffic from users on
a customizable blacklist and lets the rest pass through.

I'm sure this will be a controversial idea among free speech absolutists, but
it should ultimately be a podmin's choice to block objectionable users from
their own pod.


## Usage

Assuming a basic setup where Diaspora* and the front-end web server run on the
same host, all you need to do is point dreipfeil at the diaspora server, then
point your web server at dreipfeil.

By default, Diaspora* listens on port 3000 and dreipfeil listens on 3001, so:
> ./proxy.py --diaspora-port 3000 --bind-port 3001

Then, in your web server config, add the following to your virtualhost:

# nginx

    location ~ ^/receive/public/?$ {
      if ($request_method = POST){
        proxy_pass http://localhost:3001;
      }
    }

If you're using a Debian-based distro, you can run `install.sh` to set
everything up (minus the web server config).

That's it! Now anything from users in your blacklist.txt file will be tossed in
the garbage heap of history, and everything else will work as expected.
